import tornado.web
import tornado.httpserver
import tornado.ioloop
import tornado.websocket

from uuid import uuid4

import time

from tornado.options import define, options
define("port", default=8000, help="run the application on give port", type=int)

class MyTimer(object):
	def __init__(self, seconds):
		self.seconds = seconds

	def __tick(self):
		if self.seconds:
			self.seconds-=1
			tornado.ioloop.IOLoop.instance().add_timeout(time.time() + 1, lambda: self.__tick())

	def start(self):
		self.__tick()

	def getRemain(self):
		return self.seconds

class Positioners(object):

	def __init__(self):
		self.positioners = []

	def addPositioner(self, callback):
		self.positioners.append(callback)

	def removePositioner(self, callback):
		if callback in self.positioners:
			self.positioners.remove(callback)

	def notifyPositioners(self, id, positioner):
		for c in self.positioners:
			c(id, positioner)

class Racers(object):
	
	notifiedText = False

	def __init__(self):
		self.racers = {}
		
	def addRacer(self, callback, session):
		if session in self.racers:
			return
		self.racers[session] = callback
		# self.racers.append(callback)
		self.notifyRacers(False)

	def removeRacer(self, callback):
		session = None
		for ss, c in self.racers.iteritems():
			if c == callback:
				session = ss
				break

		if session is not None:
			del self.racers[session]
		
		# self.racers.remove(callback)
		self.notifyRacers(False)

	def notifyRacers(self, t):
		
		if self.notifiedText:
			return
		self.notifiedText|=t

		text = ''
		if t: text = self.getText()
		
		ids = self.getRacerIds()
		for ss, c in self.racers.iteritems():
			c(text, ids)

		# count = self.getCount();
		# for c in self.racers:
		# 	c(text, count)
		
	def getText(self):
		return "here we go, here we go, ahaahahahahahahaha!!!"

	def getCount(self):
		return len(self.racers)

	def getRacerIds(self):
		ids = []
		for ss, c in self.racers.iteritems():
			ids.append(ss)

		return ids

class IndexHandler(tornado.web.RequestHandler):
	def get(self):
		self.render("index.html")

class RaceHandler(tornado.web.RequestHandler):
	def get(self):

		if (self.application.timer.getRemain() == 0):
			self.render("late.html")
			return
		
		session = uuid4()
		players = self.application.racers.getRacerIds()
		players.append(session)

		self.render("race.html", session=session, time=self.application.timer.getRemain(), players=players)

class ActionHandler(tornado.web.RequestHandler):
	def post(self):
		action = self.get_argument('action')
		if not action:
			return
		flag = False
		if (action == 'text'):
			flag = True

		self.application.racers.notifyRacers(flag)

class JoinHandler(tornado.websocket.WebSocketHandler):
	def open(self):
		#self.application.racers.addRacer(self.callback)
		pass

	def on_close(self):
		self.application.racers.removeRacer(self.callback)
		self.application.positioners.removePositioner(self.racingCallback)

	def on_message(self, message):
		parsed = tornado.escape.json_decode(message)
		action = parsed['action']
		if action == 'authenticate':
			session = parsed['session']
			self.application.racers.addRacer(self.callback, session)
			self.application.positioners.addPositioner(self.racingCallback)
		elif action == 'racing':
			self.application.positioners.notifyPositioners(parsed['id'], parsed['position']);

	def callback(self, text, ids):
		self.write_message('{"action":"panel", "text": "%s", "ids": "%s"}' % (text, ",".join(ids)))

	def racingCallback(self, id, position):
		self.write_message('{"action":"racing", "id": "%s", "position": "%s"}' % (id, position))

class Application(tornado.web.Application):

	def __init__(self):
		
		self.racers = Racers()
		self.positioners = Positioners()

		self.timer = MyTimer(30)
		self.timer.start()

		handlers = [
			(r'/', IndexHandler),
			(r'/race', RaceHandler),
			(r'/join', JoinHandler),
			(r'/action', ActionHandler),
		]

		settings = {
			'template_path': 'templates',
			'static_path': 'static',
		}

		tornado.web.Application.__init__(self, handlers, **settings)

if __name__ == '__main__':
	tornado.options.parse_command_line()

	application = Application()
	http_server = tornado.httpserver.HTTPServer(application)
	http_server.listen(options.port)
	tornado.ioloop.IOLoop.instance().start()
