$(document).ready(function(){

	setTimeout(requestMessage, 100);
	
	$('#send').click(function(e){
		$.ajax({
			url: '//localhost:8000/chat/message',
			type: 'POST',
			data: {
				message: $('textarea#message').val()
			},
			beforeSend: function(xhr, settings) {
				$('#loader').show();
			},

			success: function(data, status, xhr) {
				$("#loader").hide();
			}
		});
	});

	function requestMessage() {
		$.getJSON('//localhost:8000/chat/longpolling', {session: document.session},
			function(data, status, xhr) {
				$('#conversation').append('<div style="padding-bottom: 5px;">' + data['msg'] + '</div>');
				setTimeout(requestMessage, 0);
			}
		);
	}
});
