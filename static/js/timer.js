var words,
	i = 0,
	j = 0,
	increment = 0,
	started = false,
	WIDTH = 700;

$(document).ready(function(){
	document.session = $('#session').val()
	$('#typing').removeAttr('disabled').val('');
	setTimeout(Timer, 0);
	updater.start();

	var myPos = -350;
	
	$('#typing').live('keypress', function(e) {
		if (!started) return;
		
		if (e.charCode == words[i].charCodeAt(j)) {
			myPos+=increment;
			$("#text_panel").css('color', 'green');
			j++;
			if (j == words[i].length) {
				$('#typing').val('');
				i++;
				j = 0;
				if (i == words.length) {
					$('#typing').attr('disabled', 'disabled');
					$('#text_panel').text('Вы успешно окончили нереальный баттл за первое место :)').addClass('big');
				}
			}
		}else {
			$("#text_panel").css('color', 'red');	
		}
		
		$('#' + document.session).css('left', myPos);
		$('#hidden' + document.session).val(myPos);
	});

	function Timer() {
		var timer = parseInt($('#timer').html());
		timer--;
		if (timer < 0) {
			setTimeout(raceUpdater, 1000);
			$('#time_panel').slideUp();
			$("#typing").select();
			$.ajax({
				url: '//localhost:8000/action',
				type: 'POST',
				data: {
					session: document.session,
					action: 'text'
				},
				
				success: function(data, status, xhr) { }
			});
		}else{
			$('#timer').html(timer);
			setTimeout(Timer, 1000);	
		}
	}

	function raceUpdater() {
		var raceData = {
			'action': 'racing',
			'id': document.session,
			'position': $('#hidden' + document.session).val(),
		}
		
		updater.socket.send(JSON.stringify(raceData));
		setTimeout(raceUpdater, 1000);
	}
});

var updater = {
	socket: null,

	start: function(){
		var url = 'ws://' + location.host + '/join';
		updater.socket = new WebSocket(url);

		updater.socket.onopen = function(evt) {
			var initInfo = {
				'action': 'authenticate',
				'session': document.session,
			}
			updater.socket.send(JSON.stringify(initInfo));	
		}

		updater.socket.onmessage = function(evt) {

			var json = JSON.parse(evt.data);
			//console.log(json);
			if (json.action == 'panel') {
				updater.updatePanelInfo(json);	
			}else if (json.action == 'racing'){
				$('#' + json.id).animate({left: json.position}, 500);
			}
		}
	},

	updatePanelInfo: function(data) {
		var prev = parseInt($('#players').html());
		$('#race_field').empty();
		var ids = data.ids.split(",");
		for(var i=0;i<ids.length;i++){
			if (ids[i] == document.session) {
				var t = ids[0];
				ids[0] = document.session;
				ids[i] = t;
				break;
			}
		}

		for(var i=0;i<ids.length;i++){
			var $racer = $('<div>').addClass('road').attr('id', "road"+(ids[i])).append($('<img>').attr('src', '/static/images/car' + (i+1) + '.jpg').attr('id', ids[i]));
		 	$racer.append($('<input>').attr('type', 'hidden').attr('id', 'hidden' + ids[i]).attr('value', -WIDTH/2));
		 	$('#race_field').append($racer);
		}

		$('#players').html(ids.length);
		
		if (data.text != '') {
			$('#text_panel').append(data.text);
			$('#text_panel').slideDown();
			words = data.text.split(" ");
			var len = 0;
			for(var i=0;i<words.length;i++)
				len+=words[i].length;
			increment = WIDTH/len;
			started = true;
		}
	}
}
