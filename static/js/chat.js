$(document).ready(function(){
	
	$('#send').click(function(e){
		$.ajax({
			url: '//localhost:8000/chat/message',
			type: 'POST',
			data: {
				message: $('textarea#message').val()
			},
			beforeSend: function(xhr, settings) {
				$('#loader').show();
			},

			success: function(data, status, xhr) {
				$("#loader").hide();
			}
		});
	});

	var host = "ws://localhost:8000/chat";

	var ws = new WebSocket(host);
	ws.onopen = function(evt) { };
	ws.onmessage = function(evt){ 
		$('#conversation').append('<div style="padding-bottom: 5px;">' + $.parseJSON(evt.data)['msg'] + '</div>');
	};

	ws.onerror = function(evt) { };
});
